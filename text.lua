local o = require "object"

local tb = o.newtype()

function tb:New(x, y, z, border, font, color, bg, speed)
    self.x = x
    self.y = y
    self.z = z
    self.border = border
    if bg ~= nil then
        self.w = bg:getWidth()
    end
    --self.bb = {x,y,w,h}
    self.bb = {0,0,0,0}
    self.speed = speed
    self.elapsed = 0
    self.font = font
    self.color = color
    self.progress = -1
    self.bg = bg
end
function tb:Update(dt)
    if self.progress < 0 then
        return
    end
    self.elapsed = self.elapsed+dt
    if self.elapsed >= 1.0/self.speed then
        self.elapsed = 0
        self.progress = self.progress+1
    end
end
function tb:Draw()
    love.graphics.setColor(255,255,255)
    if self.bg ~= nil then
        love.graphics.draw(self.bg, self.x, self.y, 0, 1, 1)
    end
    if self.progress < 0 then
        return
    end
    love.graphics.setFont(self.font)
    love.graphics.setColor(20,20,20)
    if self.w ~= nil then
        love.graphics.printf(string.sub(self.text, 1, self.progress), self.x+self.border[1]+2, self.y+self.border[2]+2, self.w-2*self.border[1], 'left')
        love.graphics.setColor(unpack(self.color))
        love.graphics.printf(string.sub(self.text, 1, self.progress), self.x+self.border[1], self.y+self.border[2], self.w-2*self.border[1], 'left')
    else
        love.graphics.print(string.sub(self.text, 1, self.progress), self.x+self.border[1]+2, self.y+self.border[2]+2)
        love.graphics.setColor(unpack(self.color))
        love.graphics.print(string.sub(self.text, 1, self.progress), self.x+self.border[1], self.y+self.border[2])
    end
    if self.progress >= #self.text and self.cb ~= nil then
        self.cb()
        self.cb = nil
    end
end
function tb:Show(t, cb)
    self.progress = 1
    self.text = t
    self.cb = cb
end

return tb
