local mt = {}
local obj = require "object"

pmovie = {}

function clip(v, b, top)
    if v > top then
        return top
    end
    if v < b then
        return b
    end
    return v
end
function apply_effect(s, e)
    s.player.health = clip(s.player.health+e[1], 0, 100)
    s.player.mental = clip(s.player.mental+e[2], 0, 100)
    s.player.tired_factor = e[3]
    s.player.hunger_factor = e[4]
end

function reset_player(s)
    s.player.tired_factor = 1
    s.player.hunger_factor = 1
end

function sample(arr)
    r = math.random()
    for i, e in ipairs(arr) do
        if r <= e[2] then
            return e[1]
        end
        r = r-e[2]
    end
    return arr[#arr][1]
end


function pmovie:Update(dt)
    self.eta = self.eta-dt*self.s.clk.ts
    --print(self.eta)
    if self.eta <= 0 then
        self.s.current_action = nil
        self.s.clk.ts = 60
        reset_player(self.s)
    end
end
-- effect:
-- 1: dhealth
-- 2: dmental
-- 3: dtired
-- 4: dhunger
function pmovie:Start(s)
    self.eta = 2*60*60
    self.s = s
    s.clk.ts = 24*60

    local effects = {
        {{0, 10, 1, 1}, 0.1},
        {{0, 5, 1, 1}, 0.6},
        {{0, 0, 1.5, 1}, 0.2},
        {{0, -5, 1, 1}, 0.1}
    }

    local m = {
        "I picked one movie from my watchlist.",
        "I watched a movie which was quite popular a while back.",
        "I found a movie on a forum.",
    }

    local description = {
        {"It was really good, I almost cry.", "It was nice, I wish I can write a story that good."},
        {"It was OK, I was entertained.", "It was relaxing.", "I won't say it's good, but it's fun."},
        {"It was boring, I feel like I just wasted my time...", "... I think I fell asleep wathcing it."},
        {"It was sad, but I held my tears back.", "Somehow It makes me feel angry. I hated it."}
    }

    mx = m[math.random(1,3)]
    ex = math.random(1,4)
    apply_effect(s, effects[ex][1])
    dx = math.random(1, #description[ex])

    s.maintb:Show(mx.."\n"..description[ex][dx], function()end)
end

local pgame = {}
function pgame:Start(s)
    local m = "I decided to play some video games."
    local effects
    local ds
    if math.random() > 0.6 then
        self.eta = 5*60*60
        s.clk.ts = 60*60
        effects = {
            {{-1, 12, 1.3, 1.2}, 0.4},
            {{-2, 0, 1.5, 1.2}, 0.2},
            {{-5, 8, 1.3, 1.2}, 0.4}
        }

        ds = {
            {"I got immersed into the game, and forgot about time. I shouldn't have played for that long", "The game is really good, I can't stop playing it"},
            {"... I just can't get pass that level! I tried so many times. This game sucks."},
            {"I finished that game in one sitting, now my back hurts"}
        }
    else
        self.eta = 2*60*60
        s.clk.ts = 24*60

        effects = {
            {{0, 4, 1.1, 1}, 0.5},
            {{0, 2, 1.2, 1.1}, 0.5}
        }
        ds = {
            {"I played for a while, but it got boring really fast."},
            {"... It was really hard. So I gave up after a while."}
        }
    end
    ex = math.random(1, #effects)
    dx = math.random(1, #ds[ex])
    apply_effect(s, effects[ex][1])

    s.maintb:Show(m..ds[ex][dx], function()end)
    self.s = s
end
function pgame:Update(dt)
    self.eta = self.eta-dt*self.s.clk.ts
    --print(self.eta)
    if self.eta <= 0 then
        self.s.current_action = nil
        self.s.clk.ts = 60
        reset_player(self.s)
    end
end

local scode = {}
function scode:Update(dt)
    self.eta = self.eta-dt*self.s.clk.ts
    if self.eta <= 0 then
        self.s.current_action = nil
        self.s.clk.ts = 60
        reset_player(self.s)
        local prod = 1
        if self.s.player.mental < 40 then
            prod = self.s.player.mental/40
        end
        if self.s.player.health < 80 then
            prod = prod*self.s.player.health/80
        end
        if self.s.player.tired > 70 then
            prod = prod*(100-self.s.player.tired)/30
        end
        if self.s.player.hunger > 80 then
            prod = prod*(100-self.s.player.hunger)/20
        end
        self.s.player.skills.code = self.s.player.skills.code+10*prod
        self.s.maintb:Show("I spent some time learning programming online, ", function()
            if prod < 0.4 then
                self.s.maintb:Show("but I wasn't able to learn much.")
            elseif prod < 0.8 then
                self.s.maintb:Show("felt like I learned something.")
            else
                self.s.maintb:Show("and I learned a lot.")
            end
        end)
    end
end

function scode:Start(s)
    self.eta = 3*60*60
    self.s = s
    s.clk.ts = 40*60
    s.player.tired_factor = 1.2
    s.maintb:Show("I decided to spent sometime learning to code.")
end
local sart = {}
function sart:Update(dt)
    self.eta = self.eta-dt*self.s.clk.ts
    if self.eta <= 0 then
        self.s.current_action = nil
        self.s.clk.ts = 60
        reset_player(self.s)
        local prod = 1
        if self.s.player.mental < 40 then
            prod = self.s.player.mental/40
        end
        if self.s.player.health < 80 then
            prod = prod*self.s.player.health/80
        end
        if self.s.player.tired > 70 then
            prod = prod*(100-self.s.player.tired)/30
        end
        if self.s.player.hunger > 80 then
            prod = prod*(100-self.s.player.hunger)/20
        end
        self.s.player.skills.art = self.s.player.skills.art+10*prod
        self.s.maintb:Show("I spent some time practicing art following some guide online, ", function()
            if prod < 0.4 then
                self.s.maintb:Show("but I wasn't able to improve too much.")
            elseif prod < 0.8 then
                self.s.maintb:Show("felt like I was a little better at it now.")
            else
                self.s.maintb:Show("and it was fun.")
            end
        end)
    end
end

function sart:Start(s)
    self.eta = 3*60*60
    self.s = s
    s.clk.ts = 40*60
    s.player.tired_factor = 1.2
    s.maintb:Show("I decided to spent sometime practicing art.")
end
function mt:__call(s)
    function afn(aobj)
        return function(m)
            m:Hide()
            s.current_action = aobj
            aobj:Start(s)
        end
    end
    local actions = {
        playmovie = afn(pmovie),
        --["playmusic"] = afn(pmusic),
        playgame = afn(pgame),
        studycode = afn(scode),
        studyart = afn(sart)
    }

    return actions
end

local r = {}
setmetatable(r, mt)
return r
