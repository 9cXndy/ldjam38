local item = require "item"
local stat = {
    player = {
        health = 75,
        health_buff = {},
        mental = 25,
        mental_buff = {},
        hunger = math.random(20, 30),
        tired = 10,
        tired_factor = 1,
        hunger_factor = 1,
        awake_time = 0,
        skills = {
            art = 10,
            code = 10
        }
    },
    game_over = 0,
    date = 0,
    delivered = false,
    delivery = {},
    inventory = {item.cupn(), item.cupn(), item.milk()},
    saving = 500,
    fade = 100,
    current_job = nil
}

function clip(v, b, top)
    if v > top then
        return top
    end
    if v < b then
        return b
    end
    return v
end
local obj = require "object"
local o = {}
local to_draw = {}
local menu = require "menu"
local tb = require "text"
local a = require "actions"
local gm = require "gridmenu"
local actions = a(stat)
local bar = obj()
function gethp()
    ret = stat.player.health
    for i, v in pairs(stat.player.health_buff) do
        ret = ret+v
    end
    print(ret)
    return ret, ret < 10
end
function getmental()
    ret = stat.player.mental
    for i, v in pairs(stat.player.mental_buff) do
        ret = ret+v
    end
    return ret, ret < 10
end
function gethunger()
    ret = stat.player.hunger
    return ret, ret > 90
end

local ending = {}
function ending:Update(dt)
end

function opendoor(s)
    if stat.player.mental > 90 then
        stat.current_action = goodend
        o.maintb:Show("Maybe it's finally time for me to leave this small world? The door knob turned under my hand, it opened almost too easily...", function()stat.game_over=2 end)
    else
        o.maintb:Show("... I don't feel like it.")
    end
    s:Hide()
end
function bar:New(x, y, img, color, maxlen, h, fn)
    self.v = 100
    self.x = x
    self.y = y
    self.ml = maxlen
    self.h = h
    self.z = 99
    self.fn = fn
    --self.bb = {x, y, x+iw+10+self.ml, ih}
    self.bb = {0,0,0,0}
    self.c = color
    self.icon = img --love.graphics.newImage("resources/health.png")
    return self
end
function bar:Draw()
    local v
    local shake
    v, shake = self.fn()
    local dx = 0
    local dy = 0
    if shake then
        dx = math.random(-1, 1)
        dy = math.random(-1, 1)
    end
    love.graphics.setColor(255, 255, 255)

    local mm = -1
    local cic = nil
    for l, ic in pairs(self.icon) do
        if l > mm and l <= v then
            mm = l
            cic = ic
        end
    end

    if cic == nil then
        return
    end
    love.graphics.draw(cic, self.x+dx, self.y+dy, 0, 1, 1)
    iw, ih = cic:getDimensions()
    if self.ml > 0 then
        c2 = {self.c[1]*0.5, self.c[2]*.5, self.c[3]*.5}
        love.graphics.setColor(unpack(c2))
        love.graphics.rectangle('fill', self.x+iw+10, self.y+(ih-self.h)/2, self.ml, self.h)
        love.graphics.setColor(unpack(self.c))
        love.graphics.rectangle('fill', self.x+iw+10, self.y+(ih-self.h)/2, self.ml*v/100, self.h)
    end
end
function bar:Click(b)
end
function bar:Update(dt)
end
function deliver()
    print("deliver", stat.delivery[stat.date])
    if stat.delivery[stat.date] == nil then
        return
    end
    istr = {}
    for i, x in pairs(stat.delivery[stat.date]) do
        table.insert(stat.inventory, x)
        table.insert(istr, x.longn)
    end

    msg = ""
    for i, s in ipairs(istr) do
        if i == 1 then
            msg = s
        elseif i == #istr then
            msg = msg.." and "..s
        else
            msg = msg..", "..s
        end
    end

    o.maintb:Show("Your "..msg.." has been delivered.", function()end)

    stat.delivery[stat.date] = nil
end
local clock = obj.newtype()
function clock:New(x, y, t, ts, font)
    self.x = x
    self.y = y
    self.font = font
    self.t = t
    self.ts = ts
    self.bb = {0,0,0,0}--{x, y, font:getWidth("00:00"), font:getLineHeight()*font:getHeight()}
    self.z = 99
end

function clock:Draw()
    love.graphics.setColor(0,255,30)
    love.graphics.setFont(self.font)
    hr = math.floor(self.t/60/60)
    m = math.floor((self.t-hr*60*60)/60)
    timestr = string.format("%02d:%02d", hr, m)
    love.graphics.print(timestr, self.x, self.y)
end

local sleepa = {}
function clock:Update(dt)
    self.t = self.t+dt*self.ts
    if self.t >= 24*60*60 then
        self.t = 0
        stat.date = stat.date+1
        o.daydisp:Show("Day "..tostring(stat.date))
        stat.delivered = false
        for i,x in pairs(stat.inventory) do
            x:DayPass()
        end
    end
    if stat.current_action ~= sleepa then
        stat.player.awake_time = stat.player.awake_time+dt*self.ts
    else
        return
    end
    if self.t >= 12*60*60 and not stat.delivered then
        stat.delivered = true
        deliver()
    end
end

function clock:Click()
    o.maintb:Show("clock click")
end

clickable = obj.newtype()
function clickable:New(x, y, z, w, h, cb)
    self.x = x
    self.y = y
    self.bb = {x, y, w, h}
    self.cb = cb
    self.z = z
end
function clickable:Draw(b)
    return
end
function clickable:Click(b)
    self.cb(b)
end
function clickable:Update(dt)
end

function menucb(name, x, y, z, w, h, items, font, color, bg)
    o[name] = menu(x, y, z, w, h, items, font, color, bg)
    return function()
        if stat.current_action ~= nil then
            return
        end
        print("mclick "..name)
        o[name]:Show()
    end
end

local sleepse

function apply_sleep_effects(e)
    stat.player.health = clip(stat.player.health+e[1], 0, 100)
    stat.player.mental = clip(stat.player.mental+e[2], 0, 100)
    print("mental ", e[2])
    if e[4] ~= nil then
        stat.player.tired = e[4]
    else
        stat.player.tired = clip(stat.player.tired+e[5], 0, 100)
    end
end
function sleepa:Update(dt)
    if self.state == 1 then
        if stat.fade > 0 then
            stat.fade = stat.fade-2
        else
            stat.clk.ts = self.res*60*60/5
            self.state = 2
            sleepse:play()
        end
    elseif self.state == 2 then
        if not sleepse:isPlaying() then
            self.state = 3
            stat.player.awake_time = 0
            stat.player.hunger_factor = 1
            apply_sleep_effects(self.effects)
            stat.clk.ts = 60
            sleepse:rewind()
        end
    elseif self.state == 3 then
        if stat.fade < 100 then
            stat.fade = stat.fade+4
        else
            stat.current_action = nil
            -- say something about the job
            if stat.current_job ~= nil then
                if stat.date > stat.current_job.d then
                    o.maintb:Show("I wasn't able to finished job, now I felt terrible.")
                    stat.player.mental = clip(stat.player.mental-math.random(8, 15), 0, 100)
                    stat.current_job = nil
                elseif stat.current_job.d == stat.date then
                    o.maintb:Show("The deadline is today, better finish that job.")
                else
                    dd = stat.current_job.d-stat.date
                    pfix = dd > 1 and " days" or " day"
                    o.maintb:Show("The freelance job I'm doing has a deadline in "..tostring(dd)..pfix..", maybe I should spend some time on it.")
                end
            end
        end
    end
end

local sleepw = {}
function sleepw:Update(dt)
    self.eta = self.eta-dt*stat.clk.ts
    if self.eta <= 0 then
        if self.res > 0 then
            sleepa.state = 1
            sleepa.effects = self.effects
            sleepa.res = self.res
            stat.current_action = sleepa
            stat.player.hunger_factor = self.effects[3]
        else
            print("sleepw end")
            stat.clk.ts = 60
            stat.current_action = nil
            o.maintb:Show("After laying sleepless on the bed for a while, I finally decided to get up.")
            stat.player.hunger_factor = 1
            apply_sleep_effects(self.effects)
        end
    end
end
function sleepw:Start(eta, res, e, str)
    self.eta = eta
    self.res = res
    self.effects = e
    o.maintb:Show(str)
    if self.eta >= 120*60 then
        stat.clk.ts = 24*60
    else
        stat.clk.ts = self.eta/8
    end
    stat.player.hunger_factor = 0.9
    stat.current_action = self
end

function do_sleep(s)
    s:Hide()
    if stat.player.hunger > 90 then
        o.maintb:Show("I'm so hungry I don't think I will be able to sleep.")
        return
    end
    if stat.current_job ~= nil and stat.current_job.d == stat.date then
        o.maintb:Show("I have a job due today, I don't think I can go to sleep now.")
        return
    end
    if stat.player.tired > 90 then
        if stat.player.mental < 20 then
            sleepw:Start(30*60, 8,{2, 5, 0.4, 10}, -- dhealth, dmental, hungerfactor, set tired, dtired
            "Thoughts rushing through my mind while I was in bed. But I was so tired, I fell asleep quickly")
        else
            sleepw:Start(10*60, 9, {2,1,0.4,0}, "I was so tired, I fell asleep almost immediately.")
        end
    elseif stat.player.tired > 40 then
        if stat.player.mental < 20 then
            if math.random() <= 0.4 then
                sleepw:Start(240*60, 0, { 0, -4, 0.85, nil, -20},
                "I went to bed trying to sleep, but instead my thoughts just stream through my head.")
            else
                sleepw:Start(120*60, math.random(3,5), {3, 5, 0.4, 20},
                "I laid on bed sleepless for a long while. But eventually I was able to get some sleep.")
            end
        elseif stat.player.mental > 80 then
            sleepw:Start(30*60, 8, {0, 1, 0.4, 5},
            "I wasn't that tired, but with the bed comforting me, I was able to fell asleep after a while.")
        else
            sleepw:Start(math.random(45*60,60*60), math.random(7,9),{1, 2, 0.4, 5},
            "I wasn't that tired. But after laying still for a while, I started feeling sleepy.")
        end
    else
        if stat.player.mental > 80 then
            sleepw:Start(15*60, 2, {1, 1, 0.5, nil, -20},
            "I wasn't very tired. So I just took a nap.")
        else
            sleepw:Start(120*60, 0, {0, 0, 0.9, nil, -5},
            "I wasn't tired at all. Just like I thought, I couldn't fell asleep.")
        end
    end
end

local mc
local clickmap
local clickd
local scene_x
local scene_y
local scene_scale
local saved_bb = {}
local stagebg
local cursor1, cursor2
local gunse

-- TODO:
-- Take money how buy stuff (15min) [x]
-- How to get job (30min)
-- How to work (30min)
-- How to learn (15min)
-- Start screen (30min?)
-- mental after sleep? [x]
-- tiredness should increase slower [x]
function love.load()
    love.window.setMode(1080, 810)
    ww, wh = love.graphics.getDimensions()
    w = 400
    h = 300
    scene_scale = 2
    scene_x = (ww-w*scene_scale)/2
    scene_y = (wh-h*scene_scale)/2

    cursor1 = love.mouse.newCursor(love.graphics.newImage("resources/mouse.png"):getData(), 15, 15)
    cursor2 = love.mouse.newCursor(love.graphics.newImage("resources/mouses.png"):getData(), 15, 15)

    sleepse = love.audio.newSource("resources/sleep.wav", "static")
    sleepse:setLooping(false)
    gunse = love.audio.newSource("resources/g.wav", "static")
    gunse:setLooping(false)
    love.mouse.setCursor(cursor1)

    font2 = love.graphics.newImageFont("resources/fonts/font22.png", " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,!?-+/():;%&`_*#=[]'{}$", 2)
    font2:setLineHeight(0.5)

    font3= love.graphics.newImageFont("resources/fonts/font32.png", " 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-,!:()[]{}<>", 1)
    font3:setLineHeight(.6)
    font3m= love.graphics.newImageFont("resources/fonts/price.png", " 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-,!:()[]{}<>$/", 0)
    font3m:setLineHeight(.7)

    blackbg = love.graphics.newImage("resources/b.png")

    font1 = love.graphics.newImageFont("resources/fonts/font12.png", "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 :-!.,\"?>_", 0)
    font1:setLineHeight(1)

    nfont = love.graphics.newImageFont("resources/number.png", "0123456789 :", 1)

    mti = love.graphics.newImage("resources/menutop.png")
    mmi = love.graphics.newImage("resources/menu.png")
    mbi = love.graphics.newImage("resources/menubottom.png")

    tbbg = love.graphics.newImage("resources/textbox.png")
    stagebg = love.graphics.newImage("resources/stage.png")
    o.maintb = tb(0, 200, 98, {10, 12}, font1, {255,255,255}, tbbg, 20)

    o.hpb = bar(7, 7, {[0]=love.graphics.newImage("resources/health.png")}, {190,0,0}, 50, 10, gethp)
    o.mb = bar(7, 25, {
        [0] =love.graphics.newImage("resources/mlow.png"),
        [25] = love.graphics.newImage("resources/mmedium.png"),
        [85] = love.graphics.newImage("resources/mhigh.png")
    }, {242, 170, 02}, 50, 10, getmental)

    o.hb = bar(90, 7, {[0]=love.graphics.newImage("resources/hunger.png")}, {220, 220, 220}, 50, 10, gethunger)
    o.tiredb = bar(90, 25, {[0]=love.graphics.newImage("resources/tired.png")}, {66, 75, 216}, 50, 10, function()
        return stat.player.tired, stat.player.tired > 90 end)
    gmbg = {
        love.graphics.newImage("resources/gmtl.png"),
        love.graphics.newImage("resources/gmt.png"),
        love.graphics.newImage("resources/gmtr.png"),
        love.graphics.newImage("resources/gmbl.png"),
        love.graphics.newImage("resources/gmb.png"),
        love.graphics.newImage("resources/gmbr.png"),
        love.graphics.newImage("resources/gml.png"),
        love.graphics.newImage("resources/gmr.png"),
        love.graphics.newImage("resources/gmi.png")
    }

    start_time = math.random(11*60*60-20, 11*60*60+20)
    o.clk = clock(202, 70, start_time,60, nfont)
    stat.clk = o.clk
    stat.maintb = o.maintb
    o.daydisp = tb(320, 15, 98, {0, 0}, font1, {255, 255, 255}, nil, 20)
    o.daydisp:Show("Day "..tostring(stat.date))
    o.moneydisp = tb(200, 5, 98, {0, 0}, font2, {255,255,255}, nil, 20)
    o.moneydisp:Show("$"..tostring(stat.saving))
    mc = love.graphics.newCanvas(400, 300)
    mc:setFilter("linear", "nearest")
    clickmap = love.graphics.newCanvas(1080, 810)

    function buycb(n, mind, maxd)
        return function(s)
            print("buy",n)
            s:Hide()
            estd = math.random(mind, maxd)
            d = math.random(mind, maxd)
            if stat.delivery[stat.date+d] == nil then
                stat.delivery[stat.date+d] = {}
            end
            it = item[n]()
            table.insert(stat.delivery[stat.date+d], it)
            pfix = d > 1 and "s" or ""
            stat.saving = stat.saving-it.price
            o.moneydisp:Show("$"..tostring(stat.saving))
            o.maintb:Show("I ordered "..it.longn.." online. The store said it will be delivered in "..tostring(d).." day"..pfix)
        end
    end

    shopitems = {
        gm.newItem(love.graphics.newImage("resources/fruit.png"), {"fruit","$10"}, {0,0,0}, font3m, buycb("fruit", 1, 2)),
        gm.newItem(love.graphics.newImage("resources/meat.png"), {"meat", "$12"}, {0,0,0}, font3m, buycb("meat",1,2)),
        gm.newItem(love.graphics.newImage("resources/milk.png"), {"milk", "$5"}, {0,0,0}, font3m, buycb("milk",1,3)),
        gm.newItem(love.graphics.newImage("resources/veg.png"), {"vege", "$5"}, {0,0,0}, font3m, buycb("veg",1,2)),
        gm.newItem(love.graphics.newImage("resources/cupn.png"), {"noodle", "$2"}, {0,0,0}, font3m, buycb("cupn",1,2))
        --beer
        --coffee
    }
    o.shopmenu = gm(150, 100, 98, shopitems, 3, gmbg)
    o.fridgemenu = gm(77, 93, 98, {}, 3, gmbg)
    function playcb(x, y, b, t)
        print("play clicked")
    end
    function pcshop(x, y, b, t)
        o.shopmenu:Show()
    end

    function makeconsumecb(i)
        return function(s)
            ss = stat.inventory[i]:GetState()
            if ss[7] > 0 and stat.current_action ~= nil then
                o.maintb:Show("I don't want to cook now. Some quick snacks maybe?")
                return
            end
            retain = stat.inventory[i]:Consume(stat)
            if not retain then
                table.remove(stat.inventory, i)
            end
            s:Hide()
        end
    end

    empico = love.graphics.newImage("resources/empty.png")
    function fridgeclick()
        -- build menu from inventory
        fitems = {}
        if #stat.inventory <= 0 then
            for i=1,3 do
                table.insert(fitems, gm.newItem(empico, {"empty"}, {0,0,0}, font3m, function(s)s:Hide()end))
            end
        else
            for i,x in pairs(stat.inventory) do
                table.insert(fitems, gm.newItem(x.icon, x:GetDescription(), {0,0,0}, font3m, makeconsumecb(i)))
            end
        end
        o.fridgemenu.items = fitems
        o.fridgemenu:Show()
    end

    function pcjob(s)
        s:Hide()
        if stat.current_job ~= nil then
            o.maintb:Show("Let's focus on what I have on hand before getting onto something else.")
            return
        end
        r = math.random()
        sc = stat.player.skills.code
        sa = stat.player.skills.art
        if r > sc/(sc+sa) then
            diff = math.random(sa*15, sa*35)/10
            ddl = math.ceil(diff*1.5/sa)+math.random(-1, 2)
            if ddl < 3 then
                ddl = 3
            end
            stat.current_job = {art=diff, pay=math.ceil(diff*math.random(1, 3)), d=ddl+stat.date}
            o.maintb:Show("I found an art related job on a freelance website, deadline is in "..tostring(ddl).." days.")
        else
            diff = math.random(sc*15, sc*35)/10
            ddl = math.ceil(diff*1.5/sc)+math.random(-1, 2)
            if ddl < 3 then
                ddl = 3
            end
            stat.current_job = {code=diff, pay=math.ceil(diff*math.random(1, 3)),d=ddl+stat.date}
            o.maintb:Show("I took a programming gig from a freelance website, deadline is in "..tostring(ddl).." days.")
        end
    end

    function pcwork_done(worka)
        -- compute productivity
        print("asdf", stat.current_job)
        local prod = worka.spent/4.5/60/60
        if stat.player.health < 80 then
            prod = prod*stat.player.health/80
        end
        if stat.player.tired > 60 then
            prod = prod*(100-stat.player.tired)/40
        end
        local par, pco
        local m1, m2
        if stat.player.mental < 20 or prod < 0.5 then
            m1 = "I spent sometime working, but I wasn't able to concentrate."
            par = stat.player.skills.art*0.5
            pco = stat.player.skills.code*0.4
        elseif stat.player.mental < 60 or prod < 0.8 then
            m1 = "I spent sometime working on the job I took."
            local pp = 0.5+stat.player.mental/200
            par = stat.player.skills.art*pp
            pco = stat.player.skills.code*pp
        elseif stat.player.mental < 80 then
            m1="I spent sometime working, I think I was pretty productive."
            local pp = 0.2+stat.player.mental/100
            par = stat.player.skills.art*pp
            pco = stat.player.skills.code*pp
        else
            m1 = "I spent sometime working, I think I did a really good job."
            par = stat.player.skills.art*1.2
            pco = stat.player.skills.code*1.2
        end

        local left, ld
        if stat.current_job.code ~= nil then
            stat.current_job.code=stat.current_job.code-pco*prod
            left = stat.current_job.code
            ld = left/pco
        else
            stat.current_job.art = stat.current_job.art-par*prod
            left = stat.current_job.art
            ld = left/par
        end

        local payment = 0
        if left <= 0 then
            m2 = "And the job was finished! I received payment."
            payment = stat.current_job.pay
            stat.current_job = nil
        elseif ld <= 2 then
            m2 = "It should be finished in a couple of a day or two."
        else
            m2 = "... Looks like this job will take a while to finish."
        end
        o.maintb:Show(m1, function()
            o.maintb:Show(m2)
            stat.saving = stat.saving+payment
            o.moneydisp:Show("$"..tostring(stat.saving))
        end)
    end

    local worka ={}
    function worka:Update(dt)
        self.eta = self.eta-dt*stat.clk.ts
        if self.eta <= 0 then
            stat.player.hunger_factor = 1
            o.clk.ts = 60
            stat.player.health = clip(stat.player.health-self.spent*0.8/60/60, 0, 100)
            pcwork_done(self)
            stat.current_action = nil
        end
    end

    function pcwork(s)
        s:Hide()
        if stat.current_job == nil then
            o.maintb:Show("I don't have any work to do right now")
            return
        end
        worka.eta = math.random(4*60,5*60)*60
        worka.spent = worka.eta
        stat.current_action = worka
        stat.clk.ts = worka.eta/20
        stat.player.hunger_factor = 1.1
        o.maintb:Show("I think I should start working.")
    end

    pcplay = menucb("pcplaymenu", 280, 120, 98, {
        {["text"] = "movie", ["cb"] = actions.playmovie},
        {["text"] = "game", ["cb"] = actions.playgame},
    }, font3, {0,0,0}, {mti, mmi, mbi})

    pcstudy = menucb("pcstudymenu", 280, 120, 98, {
        {["text"] = "code", ["cb"] = actions.studycode},
        {["text"] = "art", ["cb"] = actions.studyart},
    }, font3, {0,0,0}, {mti, mmi, mbi})

    pcclick = menucb("pcmenu", 220, 120, 98, {
        {["text"] = "play", ["cb"] = pcplay},
        {["text"] = "shop", ["cb"] = pcshop},
        {["text"] = "study", ["cb"] = pcstudy},
        {["text"] = "work", ["cb"] = pcwork},
        {["text"] = "job", ["cb"] = pcjob}
    },
    font3, {0,0,0}, {mti, mmi, mbi})

    bedclick = menucb("bedmenu", 337, 149, 98, {
        {text = "sleep", cb = do_sleep}
    },
    font3, {0,0,0}, {mti, mmi, mbi})
    doorclick = menucb("doormenu", 155, 105, 98, {
        {text = "open", cb = opendoor}
    },
    font3, {0,0,0}, {mti, mmi, mbi})
    o.pc = clickable(227, 99, 2, 90, 51, pcclick)
    o.bed = clickable(337, 149, 2, 63, 51, bedclick)
    o.door = clickable(123, 75, 2, 67, 108, doorclick)
    o.fridge = clickable(64, 73, 2, 50, 118, fridgeclick)
end
local last_screen
local game_over_timer = 0
function update1(dt)
    if stat.game_over ~= 0 then
        game_over_timer = game_over_timer+dt
        if game_over_timer >= 5 then
            love.event.quit(0)
        end
        return
    end
    for i, d in pairs(to_draw) do
        if d.Update ~= nil then
            d:Update(dt)
        end
    end

    if stat.current_action ~= nil then
        stat.current_action:Update(dt)
    end

    local hbase = 100/5.5/60/60;
    stat.player.hunger = clip(stat.player.hunger+stat.player.hunger_factor*stat.clk.ts*hbase*dt, 0, 100);

    if stat.current_action ~= sleepa then
        local tbase = 100/20/60/60;
        stat.player.tired = clip(stat.player.tired+stat.player.tired_factor*stat.clk.ts*tbase*dt, 0, 100);
        local hfactor = 1
        local mfactor = 1
        if stat.player.hunger > 95 then
            hfactor = hfactor*1.15
        end
        if stat.player.mental < 25 then
            hfactor = hfactor*1.2
        end
        if stat.player.awake_time > 17*60*60 then
            hfactor = hfactor*1.3
            mfactor = mfactor*1.2
        end
        if hfactor > 1 then
            local hpbase = 100/5/60/60;
            stat.player.health = clip(stat.player.health-hfactor*hpbase, 0, 100)
        end
        if mfactor > 1 then
            local mbase = 100/5/60/60;
            stat.player.mental = clip(stat.player.mental-mfactor*mbase, 0, 100)
        end

        if stat.player.mental <= 0 and stat.current_action ~= ending then
            stat.current_action = ending
            o.maintb:Show("I guess it's time ...", function()
                gunse:play()
                stat.game_over = 1
            end)
        elseif stat.player.health <= 0 and stat.current_action ~= ending then
            stat.current_action = ending
            o.maintb:Show("... I didn't feel very well. everything starts to fade, I collapsed on the ground.", function()
                stat.game_over = 1
            end)
        end
    end
    --collectgarbage()
end
local last_mo = {}
function love.mousemoved(x, y, dx, dy, t)
    if clickd == nil then
        return
    end
    if stat.game_over ~= 0 then
        return
    end
    r,g,b,a = clickd:getPixel(x, y)
    --print(r,g,b,a)
    if a == 0 then
        if last_mo ~= nil then
            love.mouse.setCursor(cursor1)
        end
        if last_mo ~= nil and last_mo.MouseOut ~= nil then
            last_mo:MouseOut()
        end
        last_mo = nil
        return
    end
    if to_draw[255-r] ~= nil then
        x = (x-scene_x)/scene_scale
        y = (y-scene_y)/scene_scale
        if last_mo == nil then
            love.mouse.setCursor(cursor2)
        end
        if last_mo ~= nil and last_mo.MouseOut ~= nil then
            last_mo:MouseOut()
        end
        last_mo = to_draw[255-r]
        if to_draw[255-r].MouseOver ~= nil then
            to_draw[255-r]:MouseOver(x, y)
        end
    end
end
function love.mousepressed(x, y, b, t)
    if clickd == nil then
        return
    end
    if stat.game_over ~= 0 then
        return
    end
    r,g,b,a = clickd:getPixel(x, y)
    --print(r,g,b,a)
    if a == 0 then
        for i, d in pairs(to_draw) do
            if d.ClickOther ~= nil then
                d:ClickOther(x, y, b)
            end
        end
        return
    end
    for i, d in pairs(to_draw) do
        if i ~= 255-r and d.ClickOther ~= nil then
            d:ClickOther(x, y, b)
        end
    end
    if to_draw[255-r] ~= nil then
        x = (x-scene_x)/scene_scale
        y = (y-scene_y)/scene_scale
        if to_draw[255-r].Click ~= nil then
            to_draw[255-r]:Click(x, y, b)
        end
    end
end
 
function draw1()

    ww, wh = love.graphics.getDimensions()
    love.graphics.setCanvas(mc)

    -- Draw the room, XXX need graphics
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(stagebg, 0, 0, 0, 1, 1)
    --love.graphics.rectangle('fill', 0, 0, w, h)


    -- render the scene, ordered by z
    to_draw = {}
    curr_bb = {}
    for i, d in pairs(o) do
        table.insert(to_draw, d)
        curr_bb[i] = d.bb
    end

    table.sort(to_draw, function(a, b)
        return a.z < b.z
    end)
    for i, d in ipairs(to_draw) do
        love.graphics.push()
        d:Draw()
        love.graphics.pop()
    end

    love.graphics.setCanvas()
    if stat.game_over == 0 then
        mask = 255*stat.fade/100
    else
        mask = 255
    end
    love.graphics.setColor(mask, mask, mask)
    love.graphics.draw(mc, scene_x, scene_y, 0, scene_scale, scene_scale)

    if stat.game_over ~= 0 then
        love.graphics.setBlendMode("alpha")
        if stat.game_over == 1 then
            love.graphics.setColor(255, 0, 0, game_over_timer/5*255)
            love.graphics.rectangle('fill', 0, 0, ww, wh)
        else
            love.graphics.setColor(255,255,255,game_over_timer/4*255)
            love.graphics.rectangle('fill', 0,0,ww,wh)
        end
        return
    end
    bb_update = false
    for i, d in pairs(saved_bb) do
        if curr_bb[i][0] ~= saved_bb[i][0] or
           curr_bb[i][1] ~= saved_bb[i][1] or
           curr_bb[i][2] ~= saved_bb[i][2] or
           curr_bb[i][3] ~= saved_bb[i][3] then
            bb_update = true
            break
        end
    end
    for i, d in pairs(curr_bb) do
        if saved_bb[i] == nil then
            bb_update = true
            break
        end
    end



    -- render click map
    love.graphics.setCanvas(mc)
    love.graphics.clear(0, 0, 0, 0)
    love.graphics.setBlendMode("replace", "premultiplied")
    for i, d in ipairs(to_draw) do
        love.graphics.push()
        love.graphics.setColor(255-i, 0, 0, 100)
        love.graphics.rectangle('fill', unpack(d.bb))
        love.graphics.pop()
    end
    love.graphics.setCanvas(clickmap)
    love.graphics.clear(0, 0, 0, 0)
    love.graphics.setColor(255, 255, 255)
    love.graphics.draw(mc, scene_x, scene_y, 0, scene_scale, scene_scale)

    love.graphics.setCanvas()
    love.graphics.setBlendMode('alpha')
    --love.graphics.draw(clickmap, 0, 0, 0, 1, 1)
    if not bb_update then
        return
    end
    print("update")
    collectgarbage()
    saved_bb = curr_bb
    clickd = clickmap:newImageData()
end

love.update = update1
love.draw = draw1
