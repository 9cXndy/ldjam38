local obj = require "object"
local menu = obj.newtype()

-- w: width
-- h: height of single item
function menu:New(x, y, z, items, font, color, bg)
    self.x, self.y, self.z = x, y, z
    self.w, self.h = bg[2]:getDimensions()
    self.toph = bg[1]:getHeight()
    self.bottomh = bg[3]:getHeight()
    self.items = items
    self.font, self.color = font, color
    self.bg = bg
    self.visible = false
    self.bb = {0,0,0,0}
    self.hl = -1
end

function menu:Show()
    self.visible = true
    self.bb = {self.x, self.y, self.w, self.toph+self.bottomh+self.h*#self.items}
end

function menu:Hide()
    self.visible = false
    self.bb = {0,0,0,0}
end

function menu:Click(x, y, b)
    n = math.floor((y-self.y-self.toph)/self.h)+1
    if n > #self.items or n <= 0 then
        return
    end
    self.items[n].cb(self)
end

function menu:ClickOther(x, y, b)
    if self.visible then
        self:Hide()
    end
end

function menu:MouseOut()
    self.hl = -1
end
function menu:MouseOver(x, y, b)
    n = math.floor((y-self.y-self.toph)/self.h)+1
    if n > #self.items then
        self.hl = -1
        return
    end
    self.hl = n
end

function menu:Update()
end

function menu:Draw()
    if not self.visible then
        return
    end
    curry = self.y+self.toph
    fh = self.font:getHeight()*self.font:getLineHeight()
    offy = math.floor((self.h-fh)/2)
    love.graphics.setColor(255,255,255)
    love.graphics.draw(self.bg[1], self.x, self.y, 0, 1, 1)
    love.graphics.setFont(self.font)
    for i, x in ipairs(self.items) do
        if i == self.hl then
            love.graphics.setColor(200, 200, 200)
        else
            love.graphics.setColor(255,255,255)
        end
        love.graphics.draw(self.bg[2], self.x, curry, 0, 1, 1)
        love.graphics.setColor(unpack(self.color))
        love.graphics.printf(x.text, self.x, curry+offy, self.w, 'center')
        curry = curry+self.h
    end
    love.graphics.setColor(255,255,255)
    love.graphics.draw(self.bg[3], self.x, curry, 0, 1, 1)
end

return menu
