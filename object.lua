local m = {}
local objmt = {}
function objmt:__call(...)
    ret = {}
    self.__index = self
    self.__call = objmt.__call
    setmetatable(ret, self)
    ret:New(...)
    return ret
end
function m.newtype(base)
    ret = {}
    setmetatable(ret, objmt)
    return ret
end

setmetatable(m, objmt)
function m:New()
end

return m
