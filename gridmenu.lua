local obj = require "object"
local menu = obj.newtype()

local mitem = obj.newtype()
function mitem:New(i, t, c, f, cb)
    self.icon = i
    self.text = t
    self.font = f
    self.color = c
    self.cb = cb
end
function mitem:Draw(x, y, w, h)
    fh = math.ceil(self.font:getLineHeight()*self.font:getHeight())
    bh = math.floor(h/20)
    offy = (h-fh*#self.text-bh-self.icon:getHeight())/2
    offx = (w-self.icon:getWidth())/2

    love.graphics.draw(self.icon, x+offx, y+offy, 0, 1, 1)
    love.graphics.setColor(unpack(self.color))
    cy = y+h-bh-fh*#self.text
    love.graphics.setFont(self.font)
    for i, s in ipairs(self.text) do
        love.graphics.printf(s, x, cy, w, "center")
        cy = cy+fh
    end
end
function mitem:Click(m)
    self.cb(m)
end
function menu.newItem(icon, text, color, font, cb)
    return mitem(icon, text, color, font, cb)
end

-- w: width
-- h: height of single item
-- bg = {topleft, top, topright, bottomleft, bottom, bottomright, left, right, itembg}
function menu:New(x, y, z, items, c, bg)
    self.x, self.y, self.z = x, y, z
    self.c = c
    self.iw = bg[2]:getWidth() --item width
    self.ih = bg[7]:getHeight() --item height
    self.toph = bg[1]:getHeight()
    self.bottomh = bg[3]:getHeight()
    self.items = items
    self.font, self.color = font, color
    self.bg = bg
    self.visible = false
    self.tbh, self.lbw = bg[1]:getDimensions()
    self.bbh, self.rbw = bg[6]:getDimensions()
    self.bb = {0,0,0,0}
    self.hl = -1
    self:Refresh()
end

function menu:Refresh()
    self.r = math.floor((#self.items-1)/self.c)+1
    self._bb = {self.x,self.y,self.lbw+self.rbw+self.iw*self.c, self.tbh+self.bbh+self.ih*self.r}
end


function menu:Show()
    self:Refresh()
    self.visible = true
    self.bb = self._bb
end

function menu:Hide()
    self.visible = false
    self.bb = {0,0,0,0}
end

function menu:_getitem(x, y)
    x = x-self.lbw-self.x
    y = y-self.tbh-self.y
    r = math.floor(y/self.ih)
    c = math.floor(x/self.iw)
    print(r, c)
    n = r*self.c+c+1
    return n
end

function menu:Click(x, y, b)
    print(y, self.y, self.h)
    n = self:_getitem(x, y)
    if n > #self.items or n <= 0 then
        return
    end
    self.items[n]:Click(self)
end

function menu:ClickOther(x, y, b)
    if self.visible then
        print("co")
        self:Hide()
    end
end

function menu:MouseOut()
    self.hl = -1
end
function menu:MouseOver(x, y, b)
    print(y, self.y, self.h)
    n = self:_getitem(x, y)
    print("item", n)
    if n > #self.items then
        self.hl = -1
        return
    end
    self.hl = n
end

function menu:Update()
end

function menu:Draw()
    if not self.visible then
        return
    end
    love.graphics.setColor(255,255,255)
    love.graphics.draw(self.bg[1], self.x, self.y, 0, 1, 1)
    for i=0,self.c-1 do
        love.graphics.draw(self.bg[2], self.x+self.lbw+i*self.iw, self.y, 0, 1, 1)
    end
    love.graphics.draw(self.bg[3], self.x+self.lbw+self.c*self.iw, self.y, 0, 1, 1)
    for i=0,self.r-1 do
        curry = self.y+self.tbh+self.ih*i
        love.graphics.draw(self.bg[7], self.x, curry, 0, 1, 1)
        for j=0,self.c-1 do
            n = i*self.c+j+1
            currx = self.x+self.lbw+j*self.iw
            if n == self.hl then
                love.graphics.setColor(200, 200, 200)
            end
            love.graphics.draw(self.bg[9], currx, curry, 0, 1, 1)
            if n <= #self.items then
                love.graphics.push()
                self.items[n]:Draw(currx, curry, self.iw, self.ih)
                love.graphics.pop()
                love.graphics.setColor(255,255,255)
            end
        end
        love.graphics.draw(self.bg[8], self.x+self.lbw+self.c*self.iw, curry, 0, 1, 1)
    end
    by = self.y+self.tbh+self.ih*self.r
    love.graphics.draw(self.bg[4], self.x, by, 0, 1, 1)
    for i=0,self.c-1 do
        love.graphics.draw(self.bg[5], self.x+self.lbw+i*self.iw, by, 0, 1, 1)
    end
    love.graphics.draw(self.bg[6], self.x+self.lbw+self.c*self.iw, by, 0, 1, 1)
end

return menu
