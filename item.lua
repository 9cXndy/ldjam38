local i = {};
local obj = require "object"

local itembase = obj()
function itembase:DayPass()
    self.expire = self.expire-1
    if self.expire < -180 then
        self.expire = -180
    end
end
function itembase:GetDescription()
    if self.expire < 0 then
        return {self.n, "expired"}
    end
    if self.now == nil then
        self.now = self.max
    end
    return {self.n, tostring(self.now).."/"..tostring(self.max)}
end
function clip(v, b, top)
    if v > top then
        return top
    end
    if v < b then
        return b
    end
    return v
end
function apply_food_effects(s, e)
    s.maintb:Show(e[1])
    s.player.health = clip(s.player.health+e[2], 0, 100)
    s.player.mental = clip(s.player.mental+e[3], 0, 100)
    s.player.hunger = clip(s.player.hunger+e[4], 0, 100)
    s.player.tired = clip(s.player.tired+e[5], 0, 100)
end
local cooking = {}
function cooking:Update(dt)
    self.eta = self.eta-dt*self.s.clk.ts
    if self.eta <= 0 then
        self.s.current_action = nil
        self.s.clk.ts = 60
        self.s.player.tired_factor = 1
        self.s.player.hunger_factor = 1
        apply_food_effects(self.s, self.e)
    end
end
function itembase:GetState()
    local me = -200
    local r = nil
    for i, x in pairs(self.e) do
        if i > me and i <= self.expire then
            me = i
            r = x
        end
    end
    return r
end
function itembase:Consume(s)
    e = self:GetState()
    if self.now == nil then
        self.now = self.max+e[6]
    else
        self.now = self.now+e[6]
    end

    if e[7] > 0 then
        cooking.eta = e[7]
        cooking.e = e
        cooking.s = s
        s.current_action = cooking
        s.player.tired_factor = 1.1
        s.player.hunger_factor = 1.5
        if e[7] > 1120 then
            s.clk.ts = 300
        end
        s.maintb:Show("I started to prepare some "..self.longn)
    else
        apply_food_effects(s, e)
    end
    return self.now > 0
end
i.fruit = itembase()
function i.fruit:New()
    self.icon = love.graphics.newImage("resources/fruit.png")
    self.n = "fruits"
    self.longn = self.n
    self.max = 3
    self.expire = 5
    self.price = 10
    -- dhealth, dmental, dhunger, dtired
    self.e = {
        [-180] = {"The fruits were completely rotten, so I threw them away.", 0, 0, 0, 0, -3, 0},
        [-10] = {"Most of the fruits were rotten, I picked some that still looks fine and ate them.", 0, -1, -20, 0, -2,0},
        [-2] = {"The fruits started to rot, but it was mostly fine. I ate some.", 1, 0, -25, 0, -1,0},
        [0] = {"I ate some fruits. Felt like they will expire soon.", 3, 1, -30, 0, -1,0},
        [2] = {"I ate some fruits.", 3, 1, -30, 0, -1,0}
    }
end

i.meat = itembase()
function i.meat:New()
    self.icon = love.graphics.newImage("resources/meat.png")
    self.n = "meat"
    self.longn = self.n
    self.max = 2
    self.expire = 14
    self.eta = 30*60
    self.price = 12
    self.e = {
        [-180] = {"The meat was completely rotten, so I threw them away.", 0, 0, 0, 0, -2,0},
        [-10] = {"The meat had been kept frozen for too long, tastes pretty bad.", -4, -1, -40, 0, -1,30*60},
        [-2] = {"The meat tastes funny.", -3, 0, -45, 0, -1,30*60},
        [0] = {"I ate some cooked meat. It was OK, but seems it will expire soon.", -1, 2, -50, 0, -1,30*60},
        [2] = {"I ate some cooked meat. It was pretty good.", -1, 3, -50, 0, -1,30*60}
    }
end
i.milk = itembase()
function i.milk:New()
    self.icon = love.graphics.newImage("resources/milk.png")
    self.n = "milk"
    self.longn = self.n
    self.max = 5
    self.expire = 9
    self.price = 5
    self.e = {
        [-180] = {"The milk has become completely solid... I don't want to try it.", 0, 0, 0, 0, -5,0},
        [-10] = {"The milk was a bit sour. I drank it anyway", -5, -3, -10, 0, -1,0},
        [-2] = {"The milk was OK. I drank some", 0, 0, -15, 0, -1,0},
        [0] = {"I drank some milk. Seems it will expire soon.", 2, 0, -18, 0, -1,0},
        [2] = {"I drank some milk.", 2, 0, -18, 0, -1,0}
    }
end
i.veg = itembase()
function i.veg:New()
    self.icon = love.graphics.newImage("resources/veg.png")
    self.n = "vege"
    self.longn = "vegetables"
    self.max = 3
    self.expire = 4
    self.price = 5
    self.e = {
        [-180] = {"The vegetables were completely rotten, so I threw them away.", 0, 0, 0, 0, -1,20*60},
        [-2] = {"The vegetables were shrivelled up. But I ate them anyway", 0, 0, -25, 0, -1,20*60},
        [0] = {"The vegetables didn't look too good. Better eat them up soon.", 1, 0, -28, 0, -1,20*60},
        [2] = {"I ate some cooked vegetables.", 2, 1, -30, 0, -1,20*60}
    }
end
i.cupn = itembase()
function i.cupn:New()
    self.icon = love.graphics.newImage("resources/cupn.png")
    self.n = "noodle"
    self.longn = "cup noodle"
    self.max = 1
    self.expire = 180
    self.price = 2
    self.e = {
        [-180] = {"The cup noodle was way pass the expiration date, but somehow it looks fine....", -11, -2, -40, 0, -1,6*60},
        [-10] = {"The cup noodle was good as always.", -9, 0, -40, 0, -1,6*60},
        [0] = {"The cup noodle was good as always.", -5, 0, -40, 0, -1,6*60},
    }
end

return i
